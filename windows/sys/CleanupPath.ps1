############################# INPUT #####################################
# Get System and User paths
$machinePaths = [System.Environment]::GetEnvironmentVariable('Path', 'machine').Split(';')
$userPaths = [System.Environment]::GetEnvironmentVariable('Path', 'user').Split(';')
if($userPaths -eq $null)
{
    $userPaths = new-object System.Collections.Generic.HashSet[string]
}
############################# LOGIC ####################################
# Clean Logic
# 1. Remove duplicates from Machine path and User Path. 
#    If it exists in Machine Path, should not exist in User Path.
# 2. If path exists in User space (ie. C:\Users\....)
# 3. 

# 1.
$machinePaths = [System.Collections.Generic.HashSet[string]] $machinePaths 
$userPaths = [System.Collections.Generic.HashSet[string]] $userPaths

foreach($path in $machinePaths)
{
    if($userPaths.Contains($path))
    {
        $r = $userPaths.Remove($path)
    }
}

# 2.
# Build User Path
$userProfileRegExFilter = $env:UserProfile
$userProfileRegExFilter = $userProfileRegExFilter.Replace('\', '\\')
"User profile path filter: $userProfileRegExFilter"

# Get all machine paths that match
$userPathsInMachinePath = new-object 'System.Collections.Generic.HashSet[string]'
foreach($path in $machinePaths){
    if($path -match $userProfileRegExFilter) 
    {
        $path
        $r = $userPathsInMachinePath.Add($path)
    }
}
foreach($path in $userPathsInMachinePath){   
    $userPaths.Add($path)
    $machinePaths.Remove($path)
}

#Remove system paths from user paths and add them to the system path.
$machinePathsInUserPath = new-object 'System.Collections.Generic.HashSet[string]'
foreach($path in $userPaths){
    if(-not ($path -match $userProfileRegExFilter)) 
    {
        $path
        $r = $machinePathsInUserPath.Add($path)
    }
}
foreach($path in $machinePathsInUserPath)
{   
    $userPaths.Remove($path)
    $machinePaths.Add($path)
}

################################# OUTPUTS ##################################
# Output
# Print Clean Up Result

# Set Full Paths that will be written to the registry
$fullUserPath = ""
foreach($path in $userPaths){
    $fullUserPath += $path + ";"
}

$fullMachinePath = ""
foreach($path in $machinePaths){
    $fullMachinePath += $path + ";"
}

"Full System Paths:"
$fullMachinePath

"Full User Paths:"
$fullUserPath

#[System.Environment]::SetEnvironmentVariable('Path', $fullMachinePath, 'machine')
#[System.Environment]::SetEnvironmentVariable('Path', $fullUserPath, 'user')